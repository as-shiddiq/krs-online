<?php
include("config/main.php");
include("config/routing.php");
include("config/nfw_pdf.php");
$nim=$_GET['nim'];
$sql = "SELECT * FROM tahun_akademik WHERE aktif='Y' ORDER BY id ASC";
$query = mysqli_query($koneksi, $sql) or die($sql);
if (mysqli_num_rows($query) > 0){
	$f=mysqli_fetch_array($query);
	$_tahun_ajaran = $f['tahun_akademik'];
  $tahun=$f['tahun'];
  $semester=$f['semester'];
}
else{
	$_tahun_ajaran = date('Y').'1';
  $semester=1;
  $tahun=date('Y');
}
ob_clean();
ob_start() ;
?>
<html>
  <head>
    <style>
      html{
        font-size: 11px
        padding: 0
      }
      body{
        margin: -10px 0 -20px -20px
      }
      .table{
        width: 100%;
        border-spacing: 0;
      }
      .table tr td{
        border-right: 1px solid #000;
        border-top: 1px solid #000;;vertical-align:top
      }
      .table tr td:first-child{
        border-left: 1px solid #000
      }
      .table tr:last-child td{
        border-bottom: 1px solid #000
      }
      table.no-border tr td{
        border: 0 !important
      }
      .center td,
      .center
      {
        text-align: center;
      }
      .no-border-rigt{
        border-right: 0 !important
      }
      .padding10 td{
        padding: 3px
      }
      .valign-top tr td{
        vertical-align: top
      }
    </style>
  </head>
  <body>
    <?php
    $querya = mysqli_query($koneksi, "SELECT mahasiswa.*, prodi.nama as prodi_nama,c.nama as nama_dosen FROM mahasiswa
    	LEFT JOIN prodi ON prodi.kode=mahasiswa.prodi_kode
      LEFT JOIN dosen c ON mahasiswa.npk=c.npk WHERE nim='{$nim}'");
    $field = mysqli_fetch_array($querya);
    extract($field);
    $nama_dosen=$nama_dosen;
    ?>
    <table class="table">
    <tbody>
    <tr>
      <td colspan="2" width="50px" class="no-border-rigt">Nama</td>
      <td width="1px" class="no-border-rigt">:</td>
      <td width="100px"><?=$nama?></td>
      <td colspan="5" rowspan="2" width="230px">
        <table width="100%" class="no-border">
          <tr>
            <td width="30px">
              <img src="<?=$_baseUrl?>images/logo.png" class="logo" width="30px" style="">
            </td>
            <td>
              <strong>POLITEKNIK NEGERI TANAH LAUT<br>
              KARTU RENCANA STUDI</strong>
            </td>
          </tr>
        </table>
      </td>
      <td class="no-border-rigt" width="80px">Semester : <?=$semester?></td>
      <td colspan="2">Tahun : <?=$tahun?></td>
    </tr>
    <tr>
      <td colspan="2" class="no-border-rigt">NIM</td>
      <td class="no-border-rigt">:</td>
      <td><?=$nim?></td>
      <td class="no-border-rigt" >Program Studi</td>
      <td class="no-border-rigt">:</td>
      <td width="130px"><?=$prodi_nama?></td>
    </tr>
    <tr class="center">
      <td>No.</td>
      <td>Kode</td>
      <td colspan="3">Mata Kuliah</td>
      <td>SKS</td>
      <td>Kelas</td>
      <td colspan="2">Dosen PengasuhHari</td>
      <td colspan="2">Hari</td>
      <td>Jam</td>
    </tr>
    <tr class="padding10">


    <?php
    	//if (in_array($_access, array('admin', 'mahasiswa'))) {
    		$sql = "SELECT krs.*,matakuliah.nama as matakuliah_nama,matakuliah.sks as sks, matakuliah.kode as matakuliah_kode, dosen.nama as dosen_nama, dosen.gelar as dosen_gelar,dosen_matakuliah.hari,dosen_matakuliah.jam
    			FROM krs
    			LEFT JOIN dosen_matakuliah ON krs.dosen_mk_id=dosen_matakuliah.id
    			LEFT JOIN matakuliah ON dosen_matakuliah.matakuliah_kode=matakuliah.kode
    			LEFT JOIN dosen ON dosen_matakuliah.dosen_npk=dosen.npk
    			WHERE krs.nim='{$nim}'
    			ORDER BY matakuliah.kode ASC";
    	/*
    	} else if ($_access == 'dosen') {
    		$sql = "SELECT krs.*,matakuliah.nama as matakuliah_nama, matakuliah.kode as matakuliah_kode, dosen.nama as dosen_nama, dosen.gelar as dosen_gelar
    			FROM krs
    			LEFT JOIN dosen_matakuliah ON krs.dosen_mk_id=dosen_matakuliah.id
    			LEFT JOIN matakuliah ON dosen_matakuliah.matakuliah_kode=matakuliah.kode
    			LEFT JOIN dosen ON dosen_matakuliah.dosen_npk=dosen.npk
    			LEFT JOIN dosen_wali ON dosen_wali.mahasiswa_nim=krs.nim
    			WHERE krs.nim='{$nim}' AND dosen_wali.dosen_npk='{$_username}'
    			ORDER BY matakuliah.kode ASC";
    	}*/

    	$query = mysqli_query($koneksi, $sql);
    ?>

  	<?php
      $no=1;
      $totalsks=0;
  		if (mysqli_num_rows($query) > 0):
  			while($field = mysqli_fetch_array($query)):
          $totalsks+=$field['sks'] ;
    ?>
    <td class="center"><?=$no++?></td>
    <td class="center"><?= $field['matakuliah_kode'] ?></td>
    <td colspan="3"><?= $field['matakuliah_nama'] ?></td>
    <td class="center"><?= $field['sks'] ?></td>
    <td colspan="3"><?= $field['dosen_nama'] ?>, <?= $field['dosen_gelar'] ?></td>

    <td colspan="2" class="center"><?= $field['hari'] ?></td>
    <td class="center"><?= substr($field['jam'],0,5) ?></td>
    </tr>
    <?php
  			endwhile;
  		else:
  	?>
  		<tr>
  			<td colspan="4">
  			Data tidak ditemukan
  			</td>
  		</tr>
  	<?php
  		endif;
  	?>

    <tr class="padding10">
    <td colspan="5">Jumlah SKS Semester ini<</td>
    <td class="center"><?=$totalsks?></td>
    <td colspan="5">Indeks pada Semester Berikutnya</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td colspan="6" width="350px" style="padding:3px">
      Perhatian:<br>
          1.Pengisian harus dengan Dosen Pembimbing <br>
          2. Kode dan SKS harus benar Lihat buku Pedoman/Kurikulum <br>
          3. Bila terdapat beberapa mata kuliah yang sama hari dan jamnya, mata kuliah<br> &nbsp;&nbsp;&nbsp;&nbsp;yang sah adalah mata kuliah yang pertama <br>
          4. Lembar : 1 . BAAK (putih) 2. Program Studi (Kuning)
    </td>
    <td colspan="6">
    <table class="no-border valign-top" width="100%">
      <tbody>
      <tr>
        <td class="center" width="50%">
          Mengetahui/Menyetujui<br>
          Dosen Pembimbing</br>
          <br>
          <br>
          <br>
          <br>
          <?=$nama_dosen?>

        </td>
      <td class="center">
        Mahasiswa ybs
        <br>
        <br>
        <br>
        <br>
        <br>
        <?=$nama?>
        </td>
      </tr>
      </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
  </body>
</html>
<?php
 $pdf = ob_get_clean();
 generate_pdf($pdf,'KRS-'.$nim,'A5','landscape');

?>
