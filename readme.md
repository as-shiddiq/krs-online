## APLIKASI KRS 
Aplikasi KRS Online sederhana, digunakan untuk pembuatan KRS Akademik. aplikasi ini bukan original dari saya (sumber tertera), akan tetapi ada perubahan coding sesuai dengan case yang saya tangani.  
![alt text](screenshots/home.png)

My Telegram : [Nasrullah Siddik](http://telegram.me/@as_shiddiq)  
## Original Source Code
[https://github.com/moonlandsoft/krs-online.git](https://github.com/moonlandsoft/krs-online.git)  
## Template
[Metro UI CSS](https://metroui.org.ua)  

saya ucapkan terima kasih kepada :
[moonlandsoft](https://github.com/moonlandsoft)
