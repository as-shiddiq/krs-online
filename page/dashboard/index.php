<div class="">
    <table class="table" style="font-size:12px">
        <tr>
            <td>
                <img src="<?=$_baseUrl?>images/logo.png" class="logo" width="230px">
            </td>
            <td>
                <h4>
                    INFORMASI
                </h4>
               <ol>
                   <li>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna 
                       
                   </li>
                   <li>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut 
                       
                   </li>
                   <li>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magn
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       
                   </li>
                   <li>
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       
                   </li>    
               </ol>


            </td>
        </tr>
    </table>
</div>
<!-- With icon (font) -->
<?php if ($_access == 'admin'): ?>

<div class="panel">
    <div class="heading">
        <span class="icon mif-home"></span>
        <span class="title">INFORMASI</span>
    </div>
    <div class="content">
        Pastikan semua Data Mata Kuliah di atur setiap tahunnya, agar mahasiswa dapat mengatur KRS
    </div>
</div>
<?php endif; ?>


<div class="tile-area no-padding">
    <h4>MENU UTAMA</h4>
    <div class="tile-container ">

    <?php if ($_access == 'admin'): ?>
      
        <a href="<?= $_url ?>tahun-akademik">
            <div class="tile bg-violet fg-white" data-role="tile">
                    <div class="tile-content iconic">
                    <span class="icon mif-books"></span>
                    </div>
                    <span class="tile-label">TAHUN AKADEMIk</span>
            </div>
        </a>
        <a href="<?= $_url ?>mahasiswa">
        	<div class="tile bg-indigo fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-users"></span>
                    </div>
                    <span class="tile-label">MAHASISWA</span>
        	</div>
        </a>
        <a href="<?= $_url ?>dosen">
        	<div class="tile bg-crimson fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-users"></span>
                    </div>
                    <span class="tile-label">DOSEN</span>
        	</div>
        </a>
        <a href="<?= $_url ?>matakuliah">
        	<div class="tile bg-emerald fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-books"></span>
                    </div>
                    <span class="tile-label">MATAKULIAH</span>
        	</div>
        </a>
        <a href="<?= $_url ?>program-studi">
        	<div class="tile bg-orange fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-school"></span>
                    </div>
                    <span class="tile-label">PROGRAM STUDI</span>
        	</div>
        </a>
        <a href="<?= $_url ?>krs">
            <div class="tile bg-lightBlue fg-white" data-role="tile">
                    <div class="tile-content iconic">
                    <span class="icon mif-calendar"></span>
                    </div>
                    <span class="tile-label">KRS</span>
            </div>
        </a>
        <a href="<?= $_url ?>user">
        	<div class="tile bg-magenta fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-users"></span>
                    </div>
                    <span class="tile-label">PENGGUNA</span>
        	</div>
        </a>
    <?php endif; ?>


    <?php if ($_access == 'dosen'): ?>
        <a href="<?= $_url ?>dosen/view/<?= $_username ?>">
        	<div class="tile-wide bg-crimson fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-user"></span>
                    </div>
                    <span class="tile-label">PROFIL DOSEN</span>
        	</div>
        </a>
        <a href="<?= $_url ?>krs">
        	<div class="tile-wide bg-lightBlue fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-calendar"></span>
                    </div>
                    <span class="tile-label">KRS Mahasiswa</span>
        	</div>
        </a>
        <a href="<?= $_url ?>user/change-password">
        	<div class="tile-wide bg-magenta fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-key"></span>
                    </div>
                    <span class="tile-label">CHANGE PASSWORD</span>
        	</div>
        </a>
    <?php endif; ?>


    <?php if ($_access == 'mahasiswa'): ?>
        <a href="<?= $_url ?>mahasiswa/view/<?= $_username ?>">
        	<div class="tile-wide bg-cobalt fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-user"></span>
                    </div>
                    <span class="tile-label">PROFIL MAHASISWA</span>
        	</div>
        </a>
        <a href="<?= $_url ?>krs/view/<?= $_username ?>">
        	<div class="tile-wide bg-lightBlue fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-calendar"></span>
                    </div>
                    <span class="tile-label">KRS</span>
        	</div>
        </a>
        <a href="<?= $_url ?>user/change-password">
        	<div class="tile-wide bg-magenta fg-white" data-role="tile">
        			<div class="tile-content iconic">
                    <span class="icon mif-key"></span>
                    </div>
                    <span class="tile-label">UBAH KATA SANDI</span>
        	</div>
        </a>
    <?php endif; ?>

    </div>
<br>
<div class="panel">
    <div class="heading">
        <span class="icon mif-home"></span>
        <span class="title">DAFTAR MATA KULIAH YANG TERSEDIA</span>
    </div>
    <div class="content">
       <div class="tabcontrol2" data-role="tabcontrol">
       <br>
            <ul class="tabs">
            <?php
                $sql = "SELECT * FROM prodi ORDER BY kode ASC";
                $query = mysqli_query($koneksi, $sql);
                if (mysqli_num_rows($query) > 0){
                    while($field = mysqli_fetch_array($query)){
                        echo '<li><a href="#frame_'.$field['kode'].'">'.$field['nama'].'</a></li>';
                    }
                }
            ?>
            </ul>
            <div class="frames">
             <?php
                $sql = "SELECT * FROM prodi ORDER BY kode ASC";
                $query = mysqli_query($koneksi, $sql);
                if (mysqli_num_rows($query) > 0){
                    while($field = mysqli_fetch_array($query)){
                        $prodi_kode=$field['kode'];
                        
                        echo '<div class="frame" id="frame_'.$field['kode'].'">';

                        $sql = "SELECT dosen_matakuliah.*, matakuliah.nama as matakuliah_nama,matakuliah.sks as sks, matakuliah.kode as matakuliah_kode, dosen.nama as dosen_nama,
                                 dosen.gelar as dosen_gelar 
                                FROM dosen_matakuliah 
                                LEFT JOIN matakuliah ON dosen_matakuliah.matakuliah_kode=matakuliah.kode
                                LEFT JOIN dosen ON dosen_matakuliah.dosen_npk=dosen.npk
                                WHERE matakuliah.prodi_kode='{$prodi_kode}' AND dosen_matakuliah.tahun_ajaran='{$_tahun_ajaran}'
                                ";
                        $query2= mysqli_query($koneksi, $sql);
                        ?>
                        <table class="table striped hovered border bordered">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>KODE</th>
                                    <th>Matakuliah</th>
                                    <th>SKS</th>
                                    <th>Dosen</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php
                                $i=1;
                                if (mysqli_num_rows($query2) > 0):
                                    while($field = mysqli_fetch_array($query2)):
                            ?>
                                <tr>
                                    <td><?=$i++?></td>
                                    <td><?= $field['matakuliah_kode'] ?></td>
                                    <td><?= $field['matakuliah_nama'] ?></td>
                                    <td><?= $field['sks'] ?></td>
                                    <td><?= $field['dosen_nama'] ?>, <?= $field['dosen_gelar'] ?></td>
                                </tr>
                            <?php
                                    endwhile;
                                else:
                            ?>
                                <tr>
                                    <td colspan="5">
                                    Data tidak ditemukan
                                    </td>
                                </tr>
                            <?php
                                endif;
                            ?>
                                
                            </tbody>
                        </table>
                        <?php
                        echo '</div>';
                    }
                }
            ?>
                
            </div>
        </div>  
    </div>
 </div>
 <br>

</div>