<h1>
<a href="<?= $_url ?>matakuliah/view/<?= $_id ?>" class="nav-button transform"><span></span></a>
Tambah Dosen Pengajar
</h1>

<?php
	$matakuliah_kode = $_id;
	if (isset($_POST['submit'])) {
		extract($_POST);

		$sqlin = "INSERT INTO dosen_matakuliah(dosen_npk,matakuliah_kode,tahun_ajaran,hari,jam) VALUES('{$npk}','{$matakuliah_kode}','{$_tahun_ajaran}','{$hari}','{$jam}')";
		$query = mysqli_query($koneksi, $sqlin) or die($sqlin);

		if ($query) {
			echo "<script>$.Notify({
			    caption: 'Success',
			    content: 'Data Dosen Pengajar Berhasil Ditambah',
	    		type: 'success'
			});
			setTimeout(function(){ window.location.href='{$_url}matakuliah/view/{$matakuliah_kode}'; }, 1200);
			</script>";
		} else {
			echo "<script>$.Notify({
			    caption: 'Failed',
			    content: 'Data Dosen Pengajar Gagal Ditambah',
			    type: 'alert'
			});</script>";
		}
	}


	$prodi_kode = $_params[1];

	$dosen_matakuliah = mysqli_query($koneksi, "SELECT dosen_npk FROM dosen_matakuliah WHERE matakuliah_kode='{$matakuliah_kode}' AND tahun_ajaran='{$_tahun_ajaran}'");
	$dm = array();
	while ($dosen = mysqli_fetch_array($dosen_matakuliah)) {
		$dm[] = "'{$dosen['dosen_npk']}'";
	}
	$dm = implode(',', $dm);

	$sql = "SELECT * FROM dosen
			INNER JOIN dosen_prodi ON dosen.npk=dosen_prodi.dosen_npk AND dosen_prodi.prodi_kode='{$prodi_kode}'";
	if (!empty($dm))
		$sql .= " WHERE dosen.npk NOT IN ({$dm})";

	$sql .= " ORDER BY npk ASC";

	$query = mysqli_query($koneksi, $sql);
?>

<form method="post">

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th></th>
			<th>NPK</th>
			<th>Nama</th>
			<th>Hari</th>
			<th>Jam</th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (mysqli_num_rows($query) > 0):
			while($field = mysqli_fetch_array($query)):
	?>
		<tr>
			<td><input type="radio" name="npk" value="<?= $field['npk'] ?>"></td>
			<td><?= $field['npk'] ?></td>
			<td><?= $field['nama'] ?>. <?= $field['gelar'] ?></td>
			<td>
				<select name="hari">
					<option value="">-- pilih --</option>
					<option value="Senin">Senin</option>
					<option value="Selasa">Selasa</option>
					<option value="Rabu">Rabu</option>
					<option value="Kamis">Kamis</option>
					<option value="Jumat">Jumat</option>
					<option value="Sabtu">Sabtu</option>
				</select>
			</td>
			<td>
				<input type="time" name="jam">
			</td>
		</tr>
	<?php
			endwhile;
		else:
	?>
		<tr>
			<td colspan="5">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>

	</tbody>
</table>

<button type="submit" name="submit" class="button primary">SUBMIT</button>

</form>
