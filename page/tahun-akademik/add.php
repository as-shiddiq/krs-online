<h1>
<a href="<?= $_url ?>tahun-akademik" class="nav-button transform"><span></span></a>
Tambah Program Studi
</h1>

<?php
if (isset($_POST['submit'])) {

	extract($_POST);

	$sql = "INSERT INTO tahun_akademik values(null,'{$tahun}', '{$semester}', '{$tahun}{$semester}', '{$aktif}');";
	$query = mysqli_query($koneksi, $sql) or die($sql);

	if ($query) {
		echo "<script>$.Notify({
		    caption: 'Success',
		    content: 'Data Program studi Berhasil Ditambah',
    		type: 'success'
		});</script>";
	} else {
		echo "<script>$.Notify({
		    caption: 'Failed',
		    content: 'Data Program studi Gagal Ditambah',
		    type: 'alert'
		});</script>";
	}
}
?>

<form method="post">

<div class="grid">

<div class="row cells2">
	<div class="cell">
		<label>Tahun</label>
		<div class="input-control text full-size">
			<input type="text" name="tahun">
		</div>
	</div>
</div>
<div class="row cells2">
	<div class="cell">
		<label>Semester</label>
		<div class="full-size">
		<label class="input-control radio">
			<input type="radio" name="semester" value="1">
		    <span class="check"></span>
		    <span class="caption">Genap</span>
		</label>
		<label class="input-control radio">
			<input type="radio" name="semester" value="2">
		    <span class="check"></span>
		    <span class="caption">Ganjil</span>
		</label>
		</div>
	</div>
</div>
<div class="row cells2">
	<div class="cell">
		<label>Aktif</label>
		<div class="full-size">
		<label class="input-control radio">
			<input type="radio" name="aktif" value="Y">
		    <span class="check"></span>
		    <span class="caption">Ya</span>
		</label>
		<label class="input-control radio">
			<input type="radio" name="aktif" value="T">
		    <span class="check"></span>
		    <span class="caption">Tidak</span>
		</label>
		</div>
	</div>
</div>

<div class="row cells2">
	<div class="cell">
		<button type="submit" name="submit" class="button primary">SUBMIT</button>
	</div>
</div>

</div>

</form>