<?php
$sql="SELECT * FROM tahun_akademik WHERE id='{$_id}'";
$querya = mysqli_query($koneksi, $sql);
$field = mysqli_fetch_array($querya);
extract($field);
?>
<h1>
<a href="<?= $_url ?>tahun-akademik" class="nav-button transform"><span></span></a>
Edit Tahun Akademik <br> <?= $tahun_akademik ?>
</h1>

<?php

if (isset($_POST['submit'])) {

	extract($_POST);

	$sql = "UPDATE tahun_akademik SET tahun='{$tahun}', semester='{$semester}', aktif='{$aktif}', tahun_akademik='{$tahun}{$semester}'
		 WHERE id='{$_id}'";
	$query = mysqli_query($koneksi, $sql) or die($sql);

	if ($query) {
		echo "<script>$.Notify({
		    caption: 'Success',
		    content: 'Data Program studi Berhasil Ubah',
    		type: 'success'
		});</script>";
	} else {
		echo "<script>$.Notify({
		    caption: 'Failed',
		    content: 'Data Program studi Gagal Ubah',
		    type: 'alert'
		});</script>";
	}
}
?>

<form method="post">

<div class="grid">

<div class="row cells2">
	<div class="cell">
		<label>Tahun</label>
		<div class="input-control text full-size">
			<input type="text" name="tahun" value="<?=$tahun?>">
		</div>
	</div>
</div>
<div class="row cells2">
	<div class="cell">
		<label>Semester</label>
		<div class="full-size">
		<label class="input-control radio">
			<input type="radio" name="semester" value="1" <?= $semester=='1'? 'checked' : '' ?>>
		    <span class="check"></span>
		    <span class="caption">Genap</span>
		</label>
		<label class="input-control radio">
			<input type="radio" name="semester" value="2" <?= $semester=='2'? 'checked' : '' ?>>
		    <span class="check"></span>
		    <span class="caption">Ganjil</span>
		</label>
		</div>
	</div>
</div>
<div class="row cells2">
	<div class="cell">
		<label>Aktif</label>
		<div class="full-size">
		<label class="input-control radio">
			<input type="radio" name="aktif" value="Y" <?= $aktif=='Y'? 'checked' : '' ?>>
		    <span class="check"></span>
		    <span class="caption">Ya</span>
		</label>
		<label class="input-control radio">
			<input type="radio" name="aktif" value="T" <?= $aktif=='T'? 'checked' : '' ?>>
		    <span class="check"></span>
		    <span class="caption">Tidak</span>
		</label>
		</div>
	</div>
</div>

<div class="row cells2">
	<div class="cell">
		<button type="submit" name="submit" class="button primary">SUBMIT</button>
	</div>
</div>

</div>

</form>