<h1>
<a href="<?= $_url ?>" class="nav-button transform"><span></span></a>
Tahun Akademik
<span class="place-right">
	<a href="<?= $_url ?>tahun-akademik/add" class="button">Tambah Tahun Akademik</a>
</span>
</h1>

<?php
	$sql = "SELECT * FROM tahun_akademik ORDER BY id ASC";
	$query = mysqli_query($koneksi, $sql);
?>

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>Id</th>
			<th>Tahun</th>
			<th>Semester</th>
			<th>Tahun Akademik</th>
			<th>Aktif</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (mysqli_num_rows($query) > 0):
			while($field = mysqli_fetch_array($query)):
	?>
		<tr>
			<td><?= $field['id'] ?></td>
			<td><?= $field['tahun'] ?></td>
			<td><?= $field['semester'] ?></td>
			<td><?= $field['tahun_akademik'] ?></td>
			<td><?= $field['aktif'] ?></td>
			<td>
				<div class="inline-block">
				    <button class="button mini-button dropdown-toggle">Aksi</button>
				    <ul class="split-content d-menu" data-role="dropdown">
						<li><a href="<?= $_url ?>tahun-akademik/edit/<?= $field['id'] ?>/<?= urlencode($field['tahun']) ?>">
						<span class="mif-pencil"></span> Edit</a></li>
						<li><a href="<?= $_url ?>tahun-akademik/delete/<?= $field['id'] ?>/<?= urlencode($field['tahun']) ?>">
						<span class="mif-cross"></span> Delete</a></li>
				    </ul>
				</div>
			</td>
		</tr>
	<?php
			endwhile;
		else:
	?>
		<tr>
			<td colspan="6">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>